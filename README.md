# Adrenaline

Repo containing files related to SV Adrenaline and for planning work on her.

The primary file is Electrical.drawio, which is a file we're using to
plan and document the electrical and electronics rework. It's using custom
shapes from https://gitlab.com/waterwanders/diagrams
